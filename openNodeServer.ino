#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <RFM69.h>
#include <SPIFlash.h>

// Define buffer size
#define STRING_BUFFER_SIZE 255

#define NODE_NAME "sensorNode"
#define NODE_ID "123"

// RFM related defines
#define RFM_NODE_ID 1
#define RFM_NETWORK_ID 100

#define FREQUENCY RF69_868MHZ

#define ACK_TIME 30

#define ENCRYPTIONKEY "verysecurepassword"

// Serial related defines
#define SERIAL_BAUD 115200

// Misc. defines
#define DEBUG true
#define STATICIP false

////////////////////////////////////////////////
// Data Definitions
////////////////////////////////////////////////

// Declarate and initialize mac and ip address
byte mac[] = {0x90, 0xA2, 0xDA, 0x0F, 0x68, 0x40 };

#if STATICIP
IPAddress ip(192,168,178, 234);
IPAddress dns1(192,168,178,1);
IPAddress gateway(192,168,178,1);
IPAddress subnet(255,255,255,0);
#endif

// Declarate and initialize sensor data
struct dht11Sensor{
	int id;
	int temperature;
	int humidity;
} dht11_1;

// Initialze buffer array
char buffer[STRING_BUFFER_SIZE]; 

// Create server for port 80
EthernetServer server(80);

int temperature = 19;
int humidity = 43;

RFM69 radio;

File webFile;

////////////////////////////////////////////////
// Firmware Setup
////////////////////////////////////////////////

void setup(){
#if DEBUG
	Serial.begin(9600);
#endif

	// Start the Ethernet connection and the server:
#if STATICIP
	Ethernet.begin(mac, ip, dns1, gateway, subnet);
#else
	if (Ethernet.begin(mac) == 0) {
#if DEBUG
		Serial.println("Unable to set server IP address using DHCP");
#endif
		for(;;)
			;
	}
#endif

#if DEBUG
	Serial.print("Server is at ");
	Serial.println(Ethernet.localIP()); 
#endif

	server.begin();

	if (!SD.begin(4)) {
		return;
	}

	// Setup rfm
	radio.initialize(FREQUENCY,RFM_NODE_ID,RFM_NETWORK_ID);
	radio.encrypt(ENCRYPTIONKEY);
	radio.promiscuous(promiscuousMode);

#if DEBUG
	char buff[50];
	sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
	Serial.println(buff);
	
	if (flash.initialize())
	{
		Serial.print("SPI Flash Init OK ... UniqueID (MAC): ");
		flash.readUniqueId();
		for (byte i=0;i<8;i++)
		{
			Serial.print(flash.UNIQUEID[i], HEX);
			Serial.print(' ');
		}
	}
	else
		Serial.println("SPI Flash Init FAIL! (is chip present?)");
#endif



}


////////////////////////////////////////////////
// Firmware Loop
////////////////////////////////////////////////

void loop(){
	int index = 0;
	char clientline[STRING_BUFFER_SIZE];

	// Check if a client is docked
	EthernetClient client = server.available();
	if (client){

		boolean currentLineIsBlank = true;
		index = 0;

		// Handle the docked client
		while (client.connected()){
			if (client.available()){
				char c = client.read();

				// Check for end of line
				if (c != '\n' && c != '\r') {
					clientline[index] = c;
					index++;
					if (index >= STRING_BUFFER_SIZE) 
					index = STRING_BUFFER_SIZE -1;

					continue;
				}

				clientline[index] = 0;
				char* filename = processFile(clientline);
#if DEBUG
				Serial.print("Requested: ");
				Serial.println(filename);
#endif
				////////////////////////////////////////////////
				// Ajax section
				////////////////////////////////////////////////
				if (strstr(clientline, "GET /?data=temp") != 0) {
					code200(client);
					client.println(22.4);
					break;
				}

				////////////////////////////////////////////////
				// File handling
				////////////////////////////////////////////////
				if (SD.exists(filename)) {
					code200(client);
					webFile = SD.open(filename);
					if (webFile) {
						while(webFile.available()) {
							client.write(webFile.read());
						}
						webFile.close();
					}
					break;
				} else {
					if (strlen(filename) < 2) {
						webFile = SD.open("index.html");
						if (webFile) {
							while(webFile.available()) {
								client.write(webFile.read());
							}
							webFile.close();
						}
					} else {
						code404(client);
						break;
					}
				}
				break;
			}

		}
		// Give server time to get data
		delay(1);
		// Stop client for this turn
		client.stop();

	}

}


////////////////////////////////////////////////
// Helper Methods
////////////////////////////////////////////////

void code200(EthernetClient client) {
	client.println("HTTP/1.1 200 OK");
	client.println("Content-Type: text/html");
	client.println("Connection: close");
	client.println();
}

void code404(EthernetClient client) {
	client.println("HTTP/1.1 404 Not Found");
	client.println("Content-Type: text/html");
	client.println("Connection: close");
	client.println();
	client.println("<html><head><title>404 - Not Found</title></head><body><h1>404 - Not Found</h1></body></html>");
}

char* processFile(char clientline[STRING_BUFFER_SIZE]) {
	char *filename;
	filename = clientline + 5;
	(strstr(clientline, " HTTP"))[0] = 0;
	return filename;
}